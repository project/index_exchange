<?php
function index_exchange_admin_settings($form_state) {
  $form = array();

  $form['index_exchange']['index_exchange_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Index Exchange ID'),
    '#default_value' => variable_get('index_exchange_id', ''),
    '#required' => TRUE,
    '#description' => t('The Index Exchange id in the script url.'),
  );
  return system_settings_form($form);
}
